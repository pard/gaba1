import gspread
from oauth2client.service_account import ServiceAccountCredentials
import datetime
import post_proc

def getData(sheet_id):
	scope = ['https://spreadsheets.google.com/feeds',
			'https://www.googleapis.com/auth/drive']
	creds = ServiceAccountCredentials.from_json_keyfile_name('client_secret.json', scope)
	client = gspread.authorize(creds)
	sheet = client.open_by_key(sheet_id).sheet1
	
	return sheet

def daysToSunday(day):
	sunday = 6
	day_num = day.weekday()
	diff = sunday - day_num 
	
	if diff != 0:
		return diff
	else:
		user_input = input("Create bulletin for today (0) or next week (1)?[0/1] ")
		if user_input == 0:
			return diff
		else:
			return 7

def getDate():
	now = datetime.date.today()
	until_sunday = daysToSunday(now)
	sunday = now + datetime.timedelta(days=until_sunday)
	return sunday

def serviceDateFrmt(sunday):
	month_name = sunday.strftime("%B")
	rest = sunday.strftime("%d, %Y")
	return f"{month_name} {stripZ(rest)}"

def setupDateFrmt(sunday):
	month = sunday.strftime("%m")
	if month[0] == '0': month = month[1]
	day = sunday.strftime("%d")
	if day[0] == '0': day = day[1]
	year = sunday.strftime("%Y")
	return f"{month}/{day}/{year}"

def stripZ(date_str):
	if date_str[0] == '0':
		return date_str[1:]
	else:
		return date_str

def getSuffix(num):
	if num[-1] == '1':
		return num + 'st'
	elif num[-1] == '2':
		return num + 'nd'
	elif num[-1] == '3':
		return num + 'rd'
	else:
		return num + 'th'

#def escapeChars(string):
#	if '#' in string:


def getBulletin(setup_curr_row, nursery_curr_row, service_curr_row,
				setup_next_row, nursery_next_row, service_next_row,
				sunday, next_sunday):
	DateCurr = sunday.strftime('%b')
	DateCurr += ' '
	DateCurr += getSuffix(stripZ(sunday.strftime('%d')))
	DateNext = next_sunday.strftime('%b')
	DateNext += ' '
	DateNext += getSuffix(stripZ(next_sunday.strftime('%d')))
	try:
		ChurchDocReading, ChurchDocReadingNum = service_curr_row[2].split()
		if ChurchDocReading == 'MS': ChurchDocReading = 'Membership Statement'
		ChurchDocReadingNum = ChurchDocReadingNum.replace('#', '').replace('/', ' out of ')
	except:
		ChurchDocReading = ''
		ChurchDocReadingNum = ''
	bulletin = {
			'DateCurrLong' : serviceDateFrmt(sunday),
			'DateCurr' : DateCurr,
			'DateNext' : DateNext,
			'ChurchDocReading' : ChurchDocReading,
			#'ChurchDocReadingNum' : '\\' + service_curr_row[3].strip()[1],
			'ChurchDocReadingNum' : ChurchDocReadingNum,
			'ChurchDocReadingExcerpt' : 'Placeholder',
			'ChurchDocReadingFootnote' : 'Placeholder',
			'NurseryCurr' : nursery_curr_row[1],
			'NurseryNext' : nursery_next_row[1],
			'SoundCurr' : setup_curr_row[1],
			'SoundNext' : setup_next_row[1],
			'ChairCurr' : setup_curr_row[2],
			'ChairNext' : setup_next_row[2],
			'CleanCurr' : setup_curr_row[3],
			'CleanNext' : setup_next_row[3],
			'WomensNext' : 'Placeholder',
			'WedStudyName' : 'Placeholder',
			'ThuStudyName' : 'Placeholder',
			'FirstReader' : service_curr_row[3],
			'SecondReading' : service_curr_row[5],
			'SecondReader' : service_curr_row[6],
			'ThirdReading' : service_curr_row[8],
			'ThirdReader' : service_curr_row[9],
			'FirstSong' : 'Placeholder',
			'SecondSong' : 'Placeholder',
			'ThirdSong' : 'Placeholder',
			'FourthSong' : 'Placeholder',
			'FifthSong' : 'Placeholder',
			'SixthSong' : 'Placeholder',
			'Praying' : service_curr_row[11].split('-')[0].strip(),
			'Preaching' : service_curr_row[12]
			}
			#'FirstReading' : service_curr_row[2],
	return bulletin


"""
ChurchDocReadingExcerpt = May we find grace to watch over each other\textsuperscript{19} through prayer,\textsuperscript{20} helping in sickness and distress,\textsuperscript{21} \textbf{promoting spiritual growth,}\textsuperscript{22} restraining from sin, \textsuperscript{23} and stirring up love and good deeds.}\textsuperscript{24}
ChurchDocReadingFootnote = 22. Acts 2:42; 1 Corinthians 1:9; 2 Corinthians 13:14; Ephesians 4:1-3; Philippians 2:1-4
"""

if __name__ == '__main__':
	setup = '1IsdfToMDBGJRSyshj_ioN2uqryOBL-KMbVc2co1ZMNA'
	nursery = '1bnRji_KAkC9hN9tMvp4RxmBzlLDfkJYnh5bgF653iPE'
	service = '1zgel78_sUAlxpJmr5bd45gVToRsP7KsE1HvJOx-vn3s'

	sunday = getDate()
	next_sunday = sunday + datetime.timedelta(days=7)
	setup_data = getData(setup)
	nursery_data = getData(nursery)
	service_data = getData(service)

	date_format_nursery = sunday.strftime('%m/%d/%Y')
	date_format_setup = setupDateFrmt(sunday)
	date_format_service = serviceDateFrmt(sunday)

	try:
		setup_curr_cell = setup_data.find(stripZ(date_format_setup))
	except:
		bad_date = sunday + datetime.timedelta(days=1)
		bad_date_format_setup = setupDateFrmt(bad_date)
		setup_curr_cell = setup_data.find(stripZ(bad_date_format_setup))

	nursery_curr_cell = nursery_data.find(date_format_nursery)
	service_curr_cell = service_data.find(date_format_service)

	setup_curr_row = setup_data.row_values(setup_curr_cell.row)
	nursery_curr_row = nursery_data.row_values(nursery_curr_cell.row)
	service_curr_row = service_data.row_values(service_curr_cell.row)

	date_format_nursery = next_sunday.strftime('%m/%d/%Y')
	date_format_setup = setupDateFrmt(next_sunday)
	date_format_service = serviceDateFrmt(next_sunday)

	try:
		setup_next_cell = setup_data.find(stripZ(date_format_setup))
	except:
		bad_date = sunday + datetime.timedelta(days=8)
		bad_date_format_setup = setupDateFrmt(bad_date)
		setup_next_cell = setup_data.find(stripZ(bad_date_format_setup))

	nursery_next_cell = nursery_data.find(date_format_nursery)
	service_next_cell = service_data.find(date_format_service)
	
	setup_next_row = setup_data.row_values(setup_next_cell.row)
	nursery_next_row = nursery_data.row_values(nursery_next_cell.row)
	service_next_row = service_data.row_values(service_next_cell.row)
	
	bulletin = getBulletin(setup_curr_row, nursery_curr_row, 
							service_curr_row, setup_next_row,
							nursery_next_row, service_next_row,
							sunday, next_sunday)

	bulletin = post_proc.updateNames(bulletin)
	
	for k,v in bulletin.items():
		print(f"{k} = {v}")

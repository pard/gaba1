sof = {
		1: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		2: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		3: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		4: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		5: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		6: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		7: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		8: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		9: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		10: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		11: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		12: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		13: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		14: {
			'title': 'Holy Scripture',
			'excerpt': None,
			'references': None
			},
		15: {
			'title': 'Perseverance',
			'excerpt': "All who are chosen, called, regenerated, and justified shall persevere in faith and never finally fall away (1). Perseverance is not a human accomplishment but a work of God through the indwelling presence of the Holy Spirit who creates, sustains, and nourishes a living, growing, transforming, and enduring faith in all true believers (2).",
			'references': "(1) John 5:24; 10:27-29; Romans 8:1-2; 28-39; Hebrews 3:14; 1 John 2:19\\ (2) Romans 1:16-17; Galatians 2:20; Philippians 2:13; 2 Timothy 1:12; 1 Peter 1:3-5"
			},
		16: {
			'title': 'The Church',
			'excerpt': "The local church is the body of Christ and is under the authority of Christ alone (1). Nevertheless, qualified elders represent Him as they care for His body (2). Qualified deacons are to assist the elders as needs arise (3). Each member of the church is uniquely gifted by the Holy Spirit to edify the body (4). The local church must recognize and fellowship with the universal body of Christ represented in other true churches (5).",
			'references': "(1) 1 Corinthians 12:12-14; 1 Peter 2:4-5; Matthew 16:18; Ephesians 1:22-23; 5:23-24a // (2) Acts 20:17,28; 1 Timothy 3:1-7; Titus 1:5-9; Hebrews 13:17; 1 Peter 5:2-4 // (3) Acts 6:1-4; 1 Timothy 3:8-13 // (4) 1 Corinthians 12:7,11; 14:12,26b, Romans 12:3-8; Ephesians 4:11-16; 1 Peter 4:10-11 // (5) Ecclesiastes 4:9-12; Romans 8:16-17; Galatians 3:26 (cf. 1:2); 2 Corinthians 11:28; Colossians 4:16",
			},
		17: {
			'title': 'Baptism',
			'excerpt': "Baptism is the first act of Christian obedience (1). True baptism is immersion in water (2), signifying the believer’s union with Christ in His death, burial, and resurrection to new life (3), as well as cleansing from sin (4). Baptism may only be administered to those who demonstrate repentance from sin and make a credible profession of faith in Christ (5).",
			'references': "(1) Matthew 28:19; Mark 16:16; Acts 2:38,41; 8:37-38; 9:18; 10:47-48; 16:14-15, 31-34; 18:8 \\ (2) Matthew 3:16; Mark 1:10; John 3:23; Acts 8:36-38 \\ (3) Acts 19:3-5; Romans 6:3-6; Galatians 2:20; Colossians 2:11-14 \\ (4) Acts 2:38; 22:16; 1 Corinthians 6:11; Titus 3:5; Hebrews 10:22 \\ (5) Matthew 28:19; John 1:12-13; Acts 2:38; 8:37"
			},
		18: {
			'title': "The Lord's Supper",
			'excerpt': "Following their baptism, Christians may, and indeed, must regularly partake of the Lord’s Supper (1). By breaking and eating the bread and drinking the cup, believers commemorate the suffering and death of Jesus on the cross (2). Through the Lord’s Supper, they affirm and celebrate their oneness, their separation from the world, and their fellowship with Christ in the New Covenant (3).",
			'references': "(1) Luke 22:19; 1 Corinthians 11:25 \\ (2) Luke 22:19-20; 1 Corinthians 11:26 \\ (3) Matthew 26:27-28; Acts 2:41-47; 1 Corinthians 10:16-17,21; 11:25"
			},
		19: {
			'title': "Evangelism",
			'excerpt': "It is the calling of every local church to make disciples of all nations, baptizing them in the name of the Father and of the Son and of the Holy Spirit, teaching them to obey all that Christ has commanded (1). It is a priority in evangelism to unite new believers with local churches (2).",
			'references': "(1) Matthew 9:36-38; 28:19-20; Acts 1:8; 2 Corinthians 5:18-20 \\(2) Ezekiel 34:11-14 (cf. Acts 20:28); Matthew 16:18; Acts 2:47; 1 Corinthians 3:9; Ephesians 4:14-16; 1 Peter 2:5"
			},
		20: {
			'title': 'The Return of Christ',
			'excerpt': "The Lord Jesus Christ shall come again to raise the dead bodily, both righteous and unrighteous (1). The justified shall enjoy everlasting life in the presence of God in heaven, while the unjustified shall eternally endure God’s wrath in hell (2).",
			'references': "(1) John 5:28-29; 14:3; 1 Corinthians 15:51-55; 1 Thessalonians 4:13-18; Hebrews 9:28 \\ 2) Matthew 25:31-46; 2 Thessalonians 1:8-9; Hebrews 9:27-28; Revelation 20:15; 21:4; 22:3-5"
			},
		21: {
			'title': 'The Old Covenant',
			'excerpt': "The Old Covenant, with the Law of Moses as its core (1), was revealed to the nation of Israel (2), promising earthly blessings for obedience (3), and threatening curses for disobedience (4). Its purpose was never to offer eternal life (5), but rather to govern the life and worship of the Old Testament nation of Israel (6), to reveal the extent of man’s depravity (7), and to foreshadow Christ and the New Covenant (8).",
			'references': "(1) Exodus 19:4-5; 20:1-17 (cf. Exodus 31:18; 32:15-16; 34:28-29; Deuteronomy 4:13; 5:1-22; 1 Kings 8:9,21; Hebrews 9:4); Joshua 8:30-35; 2 Chronicles 34:14-15,19,30; Ezra 3:2; Nehemiah 8:1-9; Matthew 19:3-8 (cf. Deuteronomy 24:1-4); John 1:17; Acts 15:5; Hebrews 10:28 \\ (2) Deuteronomy 4:7-8; Romans 3:1-2; Galatians 2:14b \\ (3) Exodus 19:5; Deuteronomy 7:12-22; 11:13-15,26-29; 28:1-14 \\ (4) Deuteronomy 11:16-17,26-29; 28:15-68 \\ (5) John 5:39; Romans 3:19-21; 10:1-4; Galatians 2:16; 3:19-23; Hebrews 7:19 \\ (6) Deuteronomy 4:5-6,14; 6:1-3; 10:12-13 \\ (7) Romans 3:20; 5:20; 7:7-11; Galatians 3:21-22 \\ (8) Deuteronomy 18:15-19 (cf. Acts 3:14-24; 7:37,51-53); Luke 24:44; John 5:39; Hebrews 9:6-14; 10:1; 13:11-12"
			},
		22: {
			'title': 'The New Covenant',
			'excerpt': "The New Covenant, established through the Person and redemptive work of Christ (1), provides eternal blessings which are acquired by grace through faith (2). The Old Covenant was fulfilled in Christ, thus becoming obsolete (3). God’s final words of revelation, given through Christ and His New Testament apostles and prophets (4), have become the authority concerning Christian conduct, and the interpretive lens through which the Old Testament must be understood, and applied (5).",
			'references': "(1) Luke 22:20; 1 Corinthians 5:7; Hebrews 7:22; 8:6; 9:11-26; 13:20 \\ (2) Romans 4:1-25; 6:14; Ephesians 2:8-9; Titus 3:4-7 \\ (3) Matthew 5:17-20; Romans 6:14; 7:4,6; 10:4; 2 Corinthians 3:2-11; Galatians 3:24-25; 4:21-31; Ephesians 2:14-16; Colossians 2:13-14; Hebrews 7:11-12,18-19,22; 8:1-13; 10:9b \\ (4) John 1:17-18; 13:34-35; 1 Corinthians 2:1-12; Ephesians 2:20; 3:5 (cf. John 16:12-13) \\ (5) Deuteronomy 18:15-19 (cf. John 12:49); Isaiah 42:1-4; Matthew5:22,28,32,34,39,44; 28:20a; John 13:34-35; 14:15,21,23; 15:10,14; Acts 15:5,10,22-29; Romans 14:5-6; 1 Corinthians 9:21; Ephesians 2:20 (cf. 3:5 and John 16:12-14); Ephesians 6:2-3 (cf. Deuteronomy 5:16); Colossians 2:16-17 (cf. 2 Chronicles 2:4; 8:13; 31:3; Nehemiah 10:33); Colossians 3:1-17; Hebrews 7:12; 10:28-29 (cf. Deuteronomy 18:19; John 12:47-49)"
			},
		23: {
			'title': 'The Glory of God',
			'excerpt': "Christians must live for God’s glory alone (1) through their awareness and enjoyment of His Person (2), submission to His authority (3), and reliance upon His goodness (4). In truth, all things that have or will transpire, serve to glorify God as their highest purpose, “For of Him and through Him and to Him are all things, to whom be glory forever. Amen.” (Romans 11:36)",
			'references': "(1) Leviticus 10:3; Matthew 5:16; 1 Corinthians 10:31; Ephesians 1:4-6; Philippians 1:9-11 \\ (2) Psalm 23:4; 46:1; 139:7-10,17-18; Proverbs 15:3; Jeremiah 16:17; Matthew 28:20; Hebrews 13:5 \\ (3) 1 Samuel 15:22-23; Ecclesiastes 12:13-14; Micah 6:8; John 15:8,14-16; Ephesians 2:10; Acts 5:32; Hebrews 12:28-29 \\ (4) Psalm 23; 34:7-10; Isaiah 41:10; Matthew 6:25-32; Luke 11:11-13; Acts 17:28; Romans 8:28-39; 2 Corinthians 1:3-4; Philippians 4:6-7"
			}
		}


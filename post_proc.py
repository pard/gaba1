#Post processing of data to add/cleanup sensitive data

def updateNames(bulletin):
	bulletin['WedStudyName'] = "John"
	bulletin['ThuStudyName'] = "Galatians with Josh"

	if bulletin['ChurchDocReading'] == 'SOF':
		bulletin['ChurchDocReading'] = 'Statement of Faith'
		try:
			from sof import sof
			number = int(bulletin['ChurchDocReadingNum'])
			bulletin['ChurchDocReadingNum'] = (f"\#{number} out of 23"
					f" - {sof[number]['title']}")
			bulletin['ChurchDocReadingExcerpt'] = sof[number]['excerpt']
			bulletin['ChurchDocReadingFootnote'] = sof[number]['references']
		except Exception:
			pass

	fullNames = {
			'Justin' : 'Justin Wilson',
			'Wilson' : 'Justin Wilson',
			'Shannon' : 'Shannon Rucker',
			'Lou' : 'Lou Mancari',
			'Geisler' : 'Josh Geisler',
			'Acord' : 'Josh Acord',
			'Gary' : 'Gary Trittipoe',
			'Ryan' : 'Ryan Mayfield',
			'Mayfield' : 'Ryan Mayfield',
			'Steve' : 'Steve Packer',
			'Peter' : 'Peter Fendrich',
			'peter' : 'Peter Fendrich'
			}

	if bulletin['FirstReader'] in fullNames.keys():
		bulletin['FirstReader'] = fullNames[bulletin['FirstReader']]
	if bulletin['SecondReader'] in fullNames.keys():
		bulletin['SecondReader'] = fullNames[bulletin['SecondReader']]
	if bulletin['ThirdReader'] in fullNames.keys():
		bulletin['ThirdReader'] = fullNames[bulletin['ThirdReader']]
	if bulletin['Praying'] in fullNames.keys():
		bulletin['Praying'] = fullNames[bulletin['Praying']]
	if bulletin['Preaching'] in fullNames.keys():
		bulletin['Preaching'] = fullNames[bulletin['Preaching']]

	if '&' in bulletin['NurseryCurr']:
		bulletin['NurseryCurr'] = bulletin['NurseryCurr'].replace('&', '\&')
	if '&' in bulletin['NurseryNext']:
		bulletin['NurseryNext'] = bulletin['NurseryNext'].replace('&', '\&')

	return bulletin



